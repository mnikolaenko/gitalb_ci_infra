output "id" {
  description = "Security group ID"
  value       = "${aws_security_group.gitlab_ci.id}"
}
