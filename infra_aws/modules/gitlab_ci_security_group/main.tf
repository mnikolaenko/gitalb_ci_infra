resource "aws_security_group" "gitlab_ci" {
  name        = "gitlab-ci"
  description = "Gitlab CI security group"
  vpc_id      = "${var.vpc_id}"
}

resource "aws_security_group_rule" "allow_ssh" {
  security_group_id = "${aws_security_group.gitlab_ci.id}"
  description       = "Allow SSH connections"

  type        = "ingress"
  from_port   = 22
  to_port     = 22
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "allow_all_egress" {
  security_group_id = "${aws_security_group.gitlab_ci.id}"
  description       = "Allow all outbound connections"

  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
}
