module "security_group" {
  source = "../gitlab_ci_security_group"
  vpc_id = "${var.vpc_id}"
}

data "aws_ami" "ubuntu1804" {
  most_recent = true

  filter {
    name   = "name"
    values = ["${var.ubuntu_name}"]
  }
}

resource "aws_key_pair" "gitlab_ci_key" {
  key_name   = "gitlab_ci_key"
  public_key = "${var.public_key}"
}

resource "aws_instance" "gitlab_ci" {
  ami                    = "${data.aws_ami.ubuntu1804.id}"
  instance_type          = "${var.instance_type}"
  key_name               = "${aws_key_pair.gitlab_ci_key.key_name}"
  vpc_security_group_ids = ["${module.security_group.id}"]

  tags {
    Name = "gitlab-ci"
  }
}
