variable "ubuntu_name" {
  description = "Name of ubuntu distro"
  default     = "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server*"
}

variable "instance_type" {
  description = "Gitlab CI instance type"
  default     = "t2.micro"
}

variable "public_key" {
  description = "Gitlab CI instance public key"
}

variable "vpc_id" {
  description = "VPC ID"
}
