output "public_ip" {
  value = "${aws_instance.gitlab_ci.public_ip}"
}
