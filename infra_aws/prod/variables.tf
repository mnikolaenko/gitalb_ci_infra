variable "region" {
  description = "Infra region"
  default     = "eu-central-1"
}

variable "credentials" {
  description = "Path to credentials file"
}

variable "gitlab_ci_public_key" {
  description = "Gitlab CI public ssh key"
  default     = ""
}

variable "gitlab_ci_public_key_path" {
  description = "Path to gitlab CI instance public key"
  default     = ""
}

variable "gitlab_ci_vpc" {
  description = "Gitlab CI VPC ID"
  default     = ""
}
