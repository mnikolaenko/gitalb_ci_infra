provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.credentials}"
}

data "aws_vpc" "default" {
  default = true
}

module "gitlab_ci" {
  source = "../modules/gitlab_ci"

  # Import keys from file if provided
  public_key = "${var.gitlab_ci_public_key_path != "" ? file(var.gitlab_ci_public_key_path) : var.gitlab_ci_public_key}"

  # Use default VPC if none is specified
  vpc_id = "${var.gitlab_ci_vpc == "" ? data.aws_vpc.default.id : var.gitlab_ci_vpc}"
}

terraform {
  backend "s3" {
    bucket = "mnikolaenko-terraform"
    key    = "gitlab-ci/prod/"
    region = "eu-central-1"
  }
}
