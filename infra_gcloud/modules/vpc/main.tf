resource "google_compute_firewall" "firewall_ssh" {
  network     = "default"
  name        = "allow-ssh"
  description = "Allow ssh access"

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  target_tags   = ["ssh-server"]
  source_ranges = "${var.source_ranges}"
}

resource "google_compute_firewall" "firewall_http" {
  network     = "default"
  name        = "allow-http"
  description = "Allow http traffic"

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  target_tags   = ["http-server"]
  source_ranges = "${var.source_ranges}"
}
