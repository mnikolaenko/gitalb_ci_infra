resource "google_compute_instance" "gitlab_ci" {
  name         = "gitlab-ci"
  machine_type = "n1-standard-1"
  zone         = "${var.zone}"

  tags = ["http-server", "ssh-server"]

  boot_disk {
    initialize_params {
      image = "${var.gitlab_ci_disk_image}"
      size  = 100
    }
  }

  network_interface {
    network = "default"

    access_config = {
      nat_ip = "${google_compute_address.gitlab_ci_ip.address}"
    }
  }

  metadata {
    ssh-keys = "${var.remote_user}:${file(var.public_key_path)}"
  }
}

resource "google_compute_address" "gitlab_ci_ip" {
  name = "gitlab-ci-ip"
}
