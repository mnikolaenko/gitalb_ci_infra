variable gitlab_ci_disk_image {
  description = "Disk image"
  default     = "ubuntu-1804-lts"
}

variable zone {
  description = "Zone"
  default     = "europe-west1-b"
}

variable "remote_user" {
  description = "Name of user for remote connections"
}

variable public_key_path {
  description = "Path to the file with the list of public keys used for ssh access"
}

variable "source_ranges" {
  description = "Allowed IP addresses"
  default     = ["0.0.0.0/0"]
}
