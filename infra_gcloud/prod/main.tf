provider "google" {
  version = "1.4.0"
  project = "${var.project}"
  region  = "${var.region}"
}

module "gitlab_ci" {
  source               = "../modules/gitlab_ci/"
  public_key_path      = "${var.public_key_path}"
  gitlab_ci_disk_image = "${var.gitlab_ci_disk_image}"
  zone                 = "${var.zone}"
  remote_user          = "${var.gitlab_ci_remote_user == "" ? var.default_remote_user : var.gitlab_ci_remote_user}"
}

module "vpc" {
  source = "../modules/vpc/"
}

terraform {
  backend "gcs" {
    bucket = "terraform-state-infra-226813"
    prefix = "gitlab_ci/prod"
  }
}
