variable project {
  description = "Project ID"
}

variable region {
  description = "Region"
  default     = "europe-west4"
}

variable zone {
  description = "Zone"
  default     = "europe-west4-b"
}

variable public_key_path {
  description = "Path to the file with the list of public keys used for ssh access"
}

variable gitlab_ci_disk_image {
  description = "Disk image for gitlab CI"
  default     = "ubuntu-1804-lts"
}

variable connection_source_range {
  description = "Allowed source IP addresses"
  default     = ["0.0.0.0/0"]
}

variable "default_remote_user" {
  description = "User name for ssh connections"
}

variable "gitlab_ci_remote_user" {
  description = "User name of Gtlab CI instances for ssh connections"
  default     = ""
}
