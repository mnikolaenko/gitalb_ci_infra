output "gitlab_ci_external_ip" {
  value = "${module.gitlab_ci.external_ip}"
}

output "gitlab_ci_internal_ip" {
  value = "${module.gitlab_ci.internal_ip}"
}
